# Active Learning for Surgomic Features

This git contains the code for our paper "Active Learning for Surgomic Features in Robot-Assisted Minimally Invasive Esophagectomy - A Prospective Annotation Study"

## Content
- `src/` contains the source code
- `models/` contains the trained models

## Usage

### Setup
The code is written for Python 3.8 and Pytorch 2.0.1.

Install the requirements using `pip install -r requirements.txt`.

### Inference
If you want to use the trained models for inference, modify the `src/infer_testdata.py` script to your needs.

### Frame extraction of equidisant frames
Use `src/video_util.py` to extract equidistant frames from videos. 
The script will create a folder with the name as defined in the code and save the frames as `.png` files. 
The script takes arguments as the tasks are directly defined in the code.

### Running Active Learning Rounds
Modify the `config.py` file to your needs. And include the available annotations and videos.
Use `src/run.py` to run one complete cycle of an active learning round.
The script will create a trial folder in the output directory and save the results as `.csv` files.
New frames will be extracted based on the results of the trained model.
In the study the number of frames to be extracted was defined manually by the number of equidistant frames of the new videos.

### Training the larger set of models as described in the paper
Use `src/trial_[ins|ana|bus]_eq.py` to train the large equidisant models.
Use `src/trial_[ins|ana|bus]al.py` to train the large active learning models.
The script will create a trial folder in the output directory and save the trained models there.

### Training additional models
Additional models were trained using the `trial_[ins|ana|bus]_[all|deterministic].py` scripts.
The `all` scripts train the models on all available annotations.
The `deterministic` scripts train a non-bayesian and therefore deterministic model.


## Citing
If you use this work please cite:

```
@article{brandenburg2023active,
  title={Active learning for extracting surgomic features in robot-assisted minimally invasive esophagectomy: a prospective annotation study},
  author={Brandenburg, Johanna M and Jenke, Alexander C and Stern, Antonia and Daum, Marie TJ and Schulze, Andr{\'e} and Younis, Rayan and Petrynowski, Philipp and Davitashvili, Tornike and Vanat, Vincent and Bhasker, Nithya and others},
  journal={Surgical Endoscopy},
  pages={1--17},
  year={2023},
  publisher={Springer}
}
```
