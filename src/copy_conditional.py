#! ../venv/bin/python3

import os
from argparse import ArgumentParser

from tqdm import tqdm


def main(round: str,
         mode: str,
         src="/path/to/data/Frames/",
         dst="/cvat_share/surgomics/al_study/"  # share frames with cvat
         ):
    idx_eq = {f for f in os.listdir(os.path.join(src, round, "Equidistant")) if f.endswith(".png")}
    idx_al = {f for f in os.listdir(os.path.join(src, round, "ActiveLearning", mode)) if f.endswith(".png")}

    idx_new = idx_al - idx_eq

    os.system(f"ssh tso-anno mkdir {os.path.join(dst, round, 'ActiveLearning', mode)}")
    for frame in tqdm(idx_new, desc="Copy Frame"):
        cmd = f"scp {os.path.join(src, round, 'ActiveLearning', mode, frame)} tso-anno:{os.path.join(dst, round, 'ActiveLearning', mode, frame)}"
        os.system(cmd)

    os.system(f"ssh tso-anno chmod -R +r {os.path.join(dst, round, 'ActiveLearning', mode)}")  # copy files to annotation server running cvat


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-r", "--round", type=str, required=True, help="Which round should be copied")
    parser.add_argument("-m", "--mode", type=str, required=True,
                        help="Which mode should be cpoied ('ana' | 'bus' | 'ins')")
    args = parser.parse_args()
    main(args.round, args.mode)
