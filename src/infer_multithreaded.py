#! ../venv/bin/python3

import os
import os.path
import time
from argparse import ArgumentParser
from concurrent.futures import ThreadPoolExecutor
from threading import Lock
from typing import Iterable

import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

import config
from dataset import Video, MultiRaterAnnotation, transform
from model import BayesianResNet18

BATCH_SIZE = 64
DROPOUT = 0.2
NUM_SAMPLES = 10

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class WriteQueue:
    def __init__(self, file):
        self.lock = Lock()
        self.queue = []
        self.file = file

    def put(self, content: Iterable):
        with self.lock:
            self.queue.extend(content)

    def write(self):
        with self.lock:
            data = self.queue.copy()
            self.queue = []

        for vid, i, out in data:
            print(f"{vid},{str(i).zfill(8)},{out:.10f}", file=self.file)
        self.file.flush()

    def empty(self):
        with self.lock:
            return len(self.queue) == 0


def main(path: str, mode: str):
    print(device)

    # init data
    mras = [MultiRaterAnnotation(files=files, mode=mode) for files in config.annotations_d[mode]]
    videos = [Video(video=os.path.join(config.video_root, "small", video),
                    anonymization=os.path.join(config.video_root, "small", anonymization),
                    mras=mras,
                    transform=transform)
              for video, anonymization in config.videos]

    # init model
    n_classes_d = {
        'ana': 2,
        'bus': 9,
        'ins': 6,
    }
    net = BayesianResNet18(p=DROPOUT, n_classes=n_classes_d[mode])
    net.load_state_dict(torch.load(os.path.join(path, "Model.pt"), map_location=device))
    net.to(device)
    net.eval()
    out_file = open(os.path.join(path, "predictions.csv"), "a")
    queue = WriteQueue(out_file)

    with ThreadPoolExecutor(max_workers=8) as executor:
        jobs = [executor.submit(infer, video, net, queue) for video in videos]

        while any([j.running() for j in jobs]) or not queue.empty():
            queue.write()
            time.sleep(1)

        out_file.close()


def infer(video: torch.utils.data.Dataset, net: torch.nn.Module, queue: WriteQueue):
    dl = DataLoader(video, num_workers=0, shuffle=False, pin_memory=False, batch_size=BATCH_SIZE)
    sig = torch.nn.Sigmoid()

    with torch.inference_mode():
        for video, idx, frame in tqdm(dl, desc=f"{video.video}"):
            frame = frame.to(device)
            output = np.stack([sig(net(frame)).cpu().numpy() for _ in range(NUM_SAMPLES)], axis=0).var(axis=0).max(
                axis=1)
            queue.put(zip(video, idx.numpy(), output))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-p", "--path", type=str, required=True, help="Path to trial output")
    parser.add_argument("-m", "--mode", type=str, required=True,
                        help="Mode ('ana' | 'bus' | 'ins')")
    args = parser.parse_args()
    assert args.mode in ['ana', 'ins', 'bus']

    main(args.path, args.mode)
