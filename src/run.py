from argparse import ArgumentParser

import copy_conditional
import infer_multithreaded
import select_frames

if __name__ == '__main__':
    """
    Run a trial from start to finish
    
    Example:
        python3 run.py -r 2 -m ana -n 100 -s 0
        
        -r: round to be run
        -m: mode to be run ('ana' | 'bus' | 'ins')
        -n: number of frames to be selected (defined by equidistant sampling
        -s: skip steps and start at: 0: Modeltraining, 1:video inference, 2:frame selection, 3: copying (if the run crashes and needs to be restarted) 
    """
    parser = ArgumentParser()
    parser.add_argument("-r", "--round", type=int, required=True, help="")
    parser.add_argument("-m", "--mode", type=str, required=True, help="")
    parser.add_argument("-n", "--num_frames", type=int, required=True, help="Number of frames to be selected")
    parser.add_argument("-s", "--skip", type=int, default=None,
                        help="skip steps and start at: 0: Modeltraining, 1:video inference, 2:frame selection, 3: copying")
    parser.add_argument("--path", default=None, help="Manually set trial path")
    args = parser.parse_args()

    assert args.mode in ['ana', 'ins', 'bus']

    if args.skip is None or args.skip <= 0:
        # run trial
        if args.mode == 'ana':
            from trial_ana import trial

            path = trial(f"AL{str(args.round).zfill(2)}")

        elif args.mode == 'ins':
            from trial_ins import trial

            path = trial(f"AL{str(args.round).zfill(2)}")

        elif args.mode == 'bus':
            from trial_bus import trial

            path = trial(f"AL{str(args.round).zfill(2)}")
        else:
            exit()

    if args.round == 10:
        exit()  # only train for R10

    elif args.skip in [1, 2]:
        assert args.path is not None, "if we skip to inference or selection, the trial path needs to be set manually!"
        path = args.path

    if args.skip is None or args.skip <= 1:
        # infer frames
        infer_multithreaded.main(path, args.mode)

    if args.skip is None or args.skip <= 2:
        # select frames
        out = f"/path/to/data/Frames/AL{str(args.round + 1).zfill(2)}/ActiveLearning/{args.mode}/"
        select_frames.main(path, args.num_frames, out)

    if args.skip is None or args.skip <= 3:
        # copy frames
        copy_conditional.main(f"AL{str(args.round + 1).zfill(2)}", args.mode)
