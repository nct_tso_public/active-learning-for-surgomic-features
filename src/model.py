import torch.nn as nn
from torch.nn import Dropout
from torchvision.models import resnet18


class MonteCarloDropout(Dropout):
    """ Adds an always active dropout in front of a given module"""

    def __init__(self, module, p):
        super(MonteCarloDropout, self).__init__(p=p)  # init dropout
        self.wrapped_module = module

    def forward(self, x, *args, **kwargs):
        """ Applys dropout and then forwards data to the wrapped module
        :param x: input
        :param args: additional arguments forwarded to the wrapped module
        :param kwargs: additional keyword arguments  forwarded to the wrapped module
        :return: result of the wrapped module
        """
        return self.wrapped_module(
            super(MonteCarloDropout, self).forward(x),  # apply dropout on input
            *args, **kwargs)  # pass the modified input and remaining arguments to the wrapped module

    def train(self, mode: bool = True):
        """ apply .train() to the wrapped module but leave the wrapping dropout untouched"""
        self.wrapped_module.train(mode)
        return self

    def __repr__(self):
        """ Custom representation for this wrapper"""
        return 'Sequential_like(\n' \
               '  MonteCarloDropout(p={})\n' \
               '  {}\n' \
               ')'.format(self.p, repr(self.wrapped_module))


class BayesianResNet18(nn.Module):
    APPLY_TO = [nn.Linear, nn.Conv2d, nn.ConvTranspose2d]

    def bayesify(self, module):
        """ Adds an always active dropout in front of the given module if its class is listed in the apply_to list.
        :param module: module to be potentially wrapped
        :return: wrapped module / original module
        """
        for key in module._modules:
            if type(module._modules[key]) in BayesianResNet18.APPLY_TO:
                module._modules[key] = MonteCarloDropout(module._modules[key], p=self.p)

    def __init__(self, p: float, pretrained: bool = True, n_classes: int = None):
        super(BayesianResNet18, self).__init__()
        self.p = p
        self.net = resnet18(pretrained=pretrained)
        if n_classes is not None:
            self.net.fc = nn.Linear(self.net.fc.in_features, n_classes)

        self.net.apply(self.bayesify)

    def forward(self, *args, **kwargs):
        return self.net(*args, **kwargs)

    def __repr__(self):
        return repr(self.net)

    def state_dict(self, *args, **kwargs):
        return self.net.state_dict(*args, **kwargs)

    def load_state_dict(self, *args, **kwargs):
        return self.net.load_state_dict(*args, **kwargs)


if __name__ == '__main__':
    print(BayesianResNet18(p=.5).state_dict().keys())
    net = BayesianResNet18(p=.2)
    print(net.load_state_dict(BayesianResNet18(p=.3).state_dict()))
    print(net)
