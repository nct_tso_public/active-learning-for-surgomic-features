import os.path
from argparse import ArgumentParser
from collections import OrderedDict

import pandas as pd

import config
from video_utils import extract_frames

BLOCKED = 30  # seconds before and after the selected Frame are blocked

fps_d = {
    'DaVinciOesi1_ano': 60,
    'DaVinciOesi2_ano': 60,
    'DaVinciOesi3_ano': 60,
    'DaVinciOesi5_ano': 60,
    'DaVinciOesi6_anno': 60,
    'DaVinciOesi7_anno': 59.94,
    'DaVinciOesi8_anno': 59.94,
    'DaVinciOesi9_anno': 59.94,
    'DaVinciOesi10_anno': 59.94,
    'DaVinciOesi11_anno': 59.94,
    'DaVinciOesi12_anno': 59.94,
    'DD_00': 29.97,
    'DD_01': 29.97,
    'DD_02': 29.97,
    'DD_03': 29.97,
    'DD_04': 29.97,
    'DD_05': 29.97,
    'DD_06': 29.97,
    'DD_07': 29.97,
    'DD_08': 29.97,
    'DD_09': 30,
    'DD_10': 29.97,
}

video_path_d = {
    'DaVinciOesi1_ano': "HD/DaVinciOesi1_ano.mp4",
    'DaVinciOesi2_ano': "HD/DaVinciOesi2_ano.mp4",
    'DaVinciOesi3_ano': "HD/DaVinciOesi3_ano.mp4",
    'DaVinciOesi5_ano': "HD/DaVinciOesi5_ano.mp4",
    'DaVinciOesi6_anno': "HD/DaVinciOesi6_anno.MOV",
    'DaVinciOesi7_anno': "HD/DaVinciOesi7_anno.MOV",
    'DaVinciOesi8_anno': "HD/DaVinciOesi8_anno.MOV",
    'DaVinciOesi9_anno': "HD/DaVinciOesi9_anno.MOV",
    'DaVinciOesi10_anno': "HD/DaVinciOesi10_anno.mp4",
    'DaVinciOesi11_anno': "HD/DaVinciOesi11_anno.mp4",
    'DaVinciOesi12_anno': "HD/DaVinciOesi12_anno.mp4",
    'DD_00': "DD/raw/DD_00.mp4",
    'DD_01': "DD/raw/DD_01.mp4",
    'DD_02': "DD/raw/DD_02.mp4",
    'DD_03': "DD/raw/DD_03.mp4",
    'DD_04': "DD/raw/DD_04.mp4",
    'DD_05': "DD/raw/DD_05.mp4",
    'DD_06': "DD/raw/DD_06.mp4",
    'DD_07': "DD/raw/DD_07.mp4",
    'DD_08': "DD/raw/DD_08.mp4",
    'DD_09': "DD/raw/DD_09.mp4",
    'DD_10': "DD/raw/DD_10.mp4",
}


def select(path: str, n_frames: int):
    """
    Selects the frames with the highest variance
    A defined number of frames (30) around a selected frame are blocked to avoid selecting similar frames

    :param path:  path to trial output
    :param n_frames:  number of frames to be selected
    :return: None
    """
    data = pd.read_csv(os.path.join(path, "predictions.csv"), header=None).to_numpy()
    data_d = {(r[0], r[1]): r[2] for r in data}
    keys = OrderedDict()
    for k in sorted(data_d, key=lambda k: data_d[k], reverse=True):
        keys[k] = data_d[k]

    with open(os.path.join(path, "selected.csv"), 'a') as out_file:
        for i in range(n_frames):
            (video, id), variance = keys.popitem(last=False)  # pop FIFO
            print(f"{video},{id},{variance}", file=out_file)
            offset = int(BLOCKED * fps_d[video])
            for i in range(max(0, id - offset), id + offset + 1):
                try:
                    keys.pop((video, i))
                except:
                    pass
            out_file.flush()


def extract(path: str, out: str):
    """
    Extracts the selected frames from the videos
    :param path:  path to trial output
    :param out:  path to write the frames to
    :return: None
    """
    df = pd.read_csv(os.path.join(path, "selected.csv"), header=None).rename(
        columns={0: "video", 1: "id", 2: "variance"})
    os.makedirs(out, exist_ok=True)

    for video in pd.unique(df['video']):
        extract_frames(
            idx=df[df.video == video]['id'].to_list(),
            video=os.path.join(config.video_root, video_path_d[video]),
            out=out,
        )


def main(path, n_frames, out):
    """
    Selects and extracts the frames
    :param path:  path to trial output
    :param n_frames:  number of frames to be selected
    :param out:  path to write the frames to
    :return:  None
    """
    select(path, int(n_frames))
    extract(path, out)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-p", "--path", type=str, required=True, help="Path to trial output")
    parser.add_argument("-n", "--num_frames", type=int, required=True,
                        help="Number of frames to be selected")
    parser.add_argument("-o", "--out", type=str, required=True,
                        help="Path to write the frames to")
    args = parser.parse_args()

    main(args.path, args.num_frames, args.out)
