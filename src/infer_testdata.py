#! ../venv/bin/python3

import os.path

import numpy as np
import torch
from torch.utils.data import DataLoader, ConcatDataset
from tqdm import tqdm

from dataset import transform, Frames
from model import BayesianResNet18

DROPOUT = 0.2
NUM_SAMPLES = 100  # how many samples to infer to calculate the probability distribution

# image resolution for LARGE_* models
HEIGHT = 480  # 180
WIDTH = 640  # 240

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")  # use GPU if available


def infer_frames(mode: str, path: str):
    # load test frames, we had two sets of frames
    # modify this to your needs
    frames = ConcatDataset([
        Frames(folder="/path/to/data/Frames/Testdata/",
               height=HEIGHT, width=WIDTH,
               transform=transform),
        Frames(folder="/path/to/data/Frames/Testdata2/",
               height=HEIGHT, width=WIDTH,
               transform=transform),
    ])

    dl = DataLoader(frames, num_workers=8, shuffle=False, pin_memory=False,
                    batch_size=16)  # reduce batch size or NUM_SAMPLES if you run out of memory

    # number of classes depends on the subgroup of features
    n_classes = {
        'ana': 2,
        'bus': 9,
        'ins': 6,
    }[mode]

    if not os.path.isfile(os.path.join(path, "Model.pt")):
        return  # skip if no model is available

    net = BayesianResNet18(p=DROPOUT, n_classes=n_classes)
    net.load_state_dict(torch.load(os.path.join(path, "Model.pt"), map_location=device))
    net.to(device)
    net.eval()
    sig = torch.nn.Sigmoid()

    with open(os.path.join(path, "Testdata.csv"), "w") as out:
        # print csv header
        print({
                  'ana': 'Id,Mean_AzygosVein,Mean_GastricTube,Std_AzygosVein,Std_GastricTube',
                  'bus': 'Id,Mean_Blood0,Mean_Blood1,Mean_Blood2,Mean_Blood3,Mean_Blood4,Mean_Smoke0,Mean_Smoke1,Mean_Smoke2,Mean_Smoke3,Std_Blood0,Std_Blood1,Std_Blood2,Std_Blood3,Std_Blood4,Std_Smoke0,Std_Smoke1,Std_Smoke2,Std_Smoke3',
                  'ins': 'Id,Mean_VesselSealer,Mean_PermanentCauteryHook,Mean_ClipApplier,Mean_LargeClipApplier,Mean_MonopolarCurvedScissors,Mean_Suction,Std_VesselSealer,Std_PermanentCauteryHook,Std_ClipApplier,Std_LargeClipApplier,Std_MonopolarCurvedScissors,Std_Suction',
              }[mode], file=out)

        # infer frames
        with torch.inference_mode():
            for idx, frame in tqdm(dl, desc=f"{mode}"):
                frame = frame.to(device)
                # infer the batch NUM_SAMPLES times and calculate mean and std over the predictions
                stack = np.stack([sig(net(frame)).cpu().numpy() for _ in range(NUM_SAMPLES)], axis=0)
                mean = stack.mean(axis=0)
                std = stack.std(axis=0)

                # write the results per element in the batch to the csv file
                for m, s, i in zip(mean, std, idx):
                    if mode == 'ana':
                        print(f'{i},{m[0]},{m[1]},{s[0]},{s[1]}', file=out)
                    elif mode == 'bus':
                        print(
                            f'{i},{m[0]},{m[1]},{m[2]},{m[3]},{m[4]},{m[5]},{m[6]},{m[7]},{m[8]},{s[0]},{s[1]},{s[2]},{s[3]},{s[4]},{s[5]},{s[6]},{s[7]},{s[8]}',
                            file=out)
                    elif mode == 'ins':
                        print(
                            f'{i},{m[0]},{m[1]},{m[2]},{m[3]},{m[4]},{m[5]},{s[0]},{s[1]},{s[2]},{s[3]},{s[4]},{s[5]}',
                            file=out)
                out.flush()


if __name__ == '__main__':
    print(device)  # check if GPU is available

    for m in ['ana', 'bus', 'ins']:  # for each subgroup of features
        for trial in [d for d in
                      os.listdir("/path/to/repository/models/")
                      if
                      d.startswith(f"LARGE_") and d.__contains__(m)]:  # find trained models matching the requirements
            print(trial)
            path = os.path.join(
                "/path/to/repository/models/",
                trial)
            if not os.path.isfile(os.path.join(path, "Testdata.csv")):  # write results into the trail folder
                infer_frames(m, path)  # evaluate model on test data
