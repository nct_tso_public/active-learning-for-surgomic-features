import datetime
import os
from argparse import ArgumentParser
from shutil import copy2

import numpy as np
import torch
import torch.nn as nn
from sklearn.metrics import precision_recall_fscore_support, average_precision_score
from torch.optim import SGD
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

import config
from dataset import MultiRaterAnnotation, TrainSet, transform
from model import BayesianResNet18

MODE = 'ins'
THRESHOLD = .5
BATCH_SIZE = 16
NUM_WORKERS = 8
NUM_EPOCHS = 100
LEARN_RATE = 3e-3
DROPOUT = 0.2
NUM_CLASSES = 6


def trial(name):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    # load data
    mras = [MultiRaterAnnotation(files=files, mode=MODE) for files in config.annotations_d[MODE]]
    ds = TrainSet(
        mras=mras,
        subset=f'ActiveLearning/{MODE}',
        transform=transform,
    )
    dl = DataLoader(ds, shuffle=True, batch_size=BATCH_SIZE, num_workers=NUM_WORKERS, pin_memory=True)

    # init model
    net = BayesianResNet18(p=DROPOUT, n_classes=NUM_CLASSES)
    net.to(device)
    sig = torch.nn.Sigmoid()

    # init optim etc
    optimizer = SGD(params=filter(lambda p: p.requires_grad, net.parameters()),
                    lr=LEARN_RATE, weight_decay=1e-5)

    lr_scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer=optimizer,
                                                       max_lr=LEARN_RATE,
                                                       epochs=NUM_EPOCHS,
                                                       steps_per_epoch=len(dl))
    pos_weight = torch.tensor(
        np.clip(
            a=((len(ds) / np.stack(ds.labels.values()).sum(axis=0)) - 1),
            a_min=0,
            a_max=1e6
        )
    )
    criterion = nn.BCEWithLogitsLoss(pos_weight=pos_weight)
    criterion.to(device)

    # init logging
    name += f'_{MODE}_{datetime.datetime.now().strftime("%Y%m%d-%H%M")}'
    os.makedirs(f"out/{name}/", exist_ok=False)
    tb = SummaryWriter(f"out/tb/{name}")
    copy2(os.path.realpath(__file__), f"out/{name}/")
    copy2(os.path.join(os.path.dirname(os.path.realpath(__file__)), "config.py"), f"out/{name}/")

    with open(f"out/{name}/frames.txt", "w") as f:
        for frame in sorted(ds.data):
            print(frame, file=f)

    with open(f"out/{name}/annotations.txt", "w") as f:
        for files in config.annotations_d[MODE]:
            print(", ".join(files), file=f)

    # train
    for epoch in tqdm(range(NUM_EPOCHS), desc="Epoch"):
        tb.add_scalar("LR", lr_scheduler.get_last_lr()[0], global_step=epoch)

        net.train()
        y_score = []
        y_true = []
        cumulative_loss = []

        for _, frame, target in dl:
            output = net(frame.to(device))

            y_true.extend(target.numpy())
            y_score.extend(sig(output).detach().cpu().numpy())

            loss = criterion(output, target.to(device).float())
            cumulative_loss.append(loss.item())

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            lr_scheduler.step()

        y_score = np.asarray(y_score)
        y_true = np.asarray(y_true)
        cumulative_loss = np.nanmean(cumulative_loss)

        tb.add_scalar("Loss", cumulative_loss, global_step=epoch)
        ap = average_precision_score(y_true=y_true, y_score=y_score, average='macro')
        tb.add_scalar("Average Precision", ap, global_step=epoch)

        y_pred = y_score > THRESHOLD
        p, r, f, _ = precision_recall_fscore_support(y_true=y_true, y_pred=y_pred, average='macro')
        tb.add_scalar("Precision", p, global_step=epoch)
        tb.add_scalar("Recall", r, global_step=epoch)
        tb.add_scalar("F1-Score", f, global_step=epoch)

        label_names = list(config.tags_d[MODE].keys())[:NUM_CLASSES]
        p, r, f, s = precision_recall_fscore_support(y_true=y_true, y_pred=y_pred, average=None)
        tb.add_scalars("PrecisionPerTool", {k: v for k, v in zip(label_names, p)},
                       global_step=epoch)
        tb.add_scalars("RecallPerTool", {k: v for k, v in zip(label_names, r)},
                       global_step=epoch)
        tb.add_scalars("F1PerTool", {k: v for k, v in zip(label_names, f)},
                       global_step=epoch)
        tb.add_scalars("SupportPerTool", {k: v for k, v in zip(label_names, s)},
                       global_step=epoch)

    # save
    torch.save(net.state_dict(), f"out/{name}/Model.pt")
    return f"out/{name}/"


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-n", "--name", type=str, required=True, help="Trialname")
    args = parser.parse_args()

    trial(args.name)
