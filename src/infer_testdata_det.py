#! ../venv/bin/python3
import os.path

import numpy as np
import torch
from torch import nn
from torch.utils.data import DataLoader, ConcatDataset
from torchvision.models import resnet50
from tqdm import tqdm

from dataset import Video, transform, Frames
from model import BayesianResNet18

DROPOUT = 0.2
NUM_SAMPLES = 25
VIDEO_PATH = "/path/to/videos/DD_T2.mp4"

HEIGHT = 480  # 180
WIDTH = 640  # 240

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def infer_frames(mode: str, path: str):
    frames = ConcatDataset([
        Frames(folder="/path/to/data/Frames/Testdata/",
               height=HEIGHT, width=WIDTH,
               transform=transform),
        Frames(folder="/path/to/data/Frames/Testdata2/",
               height=HEIGHT, width=WIDTH,
               transform=transform),
    ])
    dl = DataLoader(frames, num_workers=8, shuffle=False, pin_memory=False, batch_size=16)
    n_classes = {
        'ana': 2,
        'bus': 9,
        'ins': 6,
    }[mode]

    if not os.path.isfile(os.path.join(path, "Model.pt")):
        return  # skip if no model is available

    net = resnet50()
    net.fc = nn.Linear(net.fc.in_features, n_classes)
    net.load_state_dict(torch.load(os.path.join(path, "Model.pt"), map_location=device))
    net.to(device)
    net.eval()
    sig = torch.nn.Sigmoid()

    with open(os.path.join(path, "Testdata.csv"), "w") as out:
        print({
                  'ana': 'Id,Mean_AzygosVein,Mean_GastricTube,Std_AzygosVein,Std_GastricTube',
                  'bus': 'Id,Mean_Blood0,Mean_Blood1,Mean_Blood2,Mean_Blood3,Mean_Blood4,Mean_Smoke0,Mean_Smoke1,Mean_Smoke2,Mean_Smoke3,Std_Blood0,Std_Blood1,Std_Blood2,Std_Blood3,Std_Blood4,Std_Smoke0,Std_Smoke1,Std_Smoke2,Std_Smoke3',
                  'ins': 'Id,Mean_VesselSealer,Mean_PermanentCauteryHook,Mean_ClipApplier,Mean_LargeClipApplier,Mean_MonopolarCurvedScissors,Mean_Suction,Std_VesselSealer,Std_PermanentCauteryHook,Std_ClipApplier,Std_LargeClipApplier,Std_MonopolarCurvedScissors,Std_Suction',
              }[mode], file=out)
        with torch.inference_mode():
            for idx, frame in tqdm(dl, desc=f"{mode}"):
                frame = frame.to(device)
                stack = np.stack([sig(net(frame)).cpu().numpy() for _ in range(NUM_SAMPLES*4)], axis=0)  # 100 samples!
                mean = stack.mean(axis=0)
                std = stack.std(axis=0)
                for m, s, i in zip(mean, std, idx):
                    if mode == 'ana':
                        print(f'{i},{m[0]},{m[1]},{s[0]},{s[1]}', file=out)
                    elif mode == 'bus':
                        print(
                            f'{i},{m[0]},{m[1]},{m[2]},{m[3]},{m[4]},{m[5]},{m[6]},{m[7]},{m[8]},{s[0]},{s[1]},{s[2]},{s[3]},{s[4]},{s[5]},{s[6]},{s[7]},{s[8]}',
                            file=out)
                    elif mode == 'ins':
                        print(
                            f'{i},{m[0]},{m[1]},{m[2]},{m[3]},{m[4]},{m[5]},{s[0]},{s[1]},{s[2]},{s[3]},{s[4]},{s[5]}',
                            file=out)
                out.flush()


if __name__ == '__main__':
    print(device)

    for m in ['ana', 'bus', 'ins']:
        for trial in [d for d in
             os.listdir("/path/to/repository/models/")
             if d.startswith(f"DET_") and d.__contains__(m)]:
            print(trial)
            path = os.path.join(
                "/path/to/repository/models/",
                trial)
            if not os.path.isfile(os.path.join(path, "Testdata.csv")):
                infer_frames(m, path)
