import logging
from os import listdir
from os.path import join
from typing import Tuple, List
from xml.etree import ElementTree as ET

import albumentations as A
import cv2
import numpy as np
import torch
import torch.nn as nn
import torchvision.transforms as transforms
from PIL import Image
from albumentations.pytorch import ToTensorV2
from torch.utils.data import Dataset, IterableDataset

import config

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
transform = transforms.Compose([transforms.ToTensor(), normalize])

base_transform = A.Compose([
    A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
    ToTensorV2(),
])

train_transform = A.Compose([
    A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.1, rotate_limit=5, p=0.5),
    A.RGBShift(r_shift_limit=20, g_shift_limit=20, b_shift_limit=20, p=0.5),
    A.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=0.5),
    base_transform,
])

HEIGHT = 480  # 180
WIDTH = 640  # 240


def ana_merger(annotations):
    tags_d = config.tags_d['ana']
    label = np.zeros(len(tags_d))
    for anno in annotations:
        for tag in anno.findall('tag'):
            name = tag.get('label')
            if name in tags_d:
                label[tags_d[name]] += 1

    label = (label / len(annotations)) >= .5
    return label[:2]  # ignore spurting bleeding


def bus_merger(annotations):
    tags_d = config.tags_d['bus']
    label = np.zeros(len(tags_d))
    for anno in annotations:
        for tag in anno.findall('tag'):
            name = tag.get('label')
            if name in tags_d:
                label[tags_d[name]] += 1

    label /= len(annotations)
    blood = label[:5]
    blood_i = np.argmax(blood)
    smoke = label[5:9]
    smoke_i = np.argmax(smoke)

    label = np.zeros(len(tags_d))
    label[blood_i] = 1
    label[smoke_i + 5] = 1

    if blood[blood_i] <= .5 or smoke[smoke_i] <= .5:
        if blood[blood_i] <= .5:
            r = "Blood"
        if smoke[smoke_i] <= .5:
            r = "Smoke"
        if blood[blood_i] <= .5 and smoke[smoke_i] <= .5:
            r = "Smoke & Blood"

        logging.warning(
            f"There is no majority among raters for image '{annotations[0].get('name').split('/')[-1]}' for {r}")

    label = label[:9]  # ignore spurting bleeding
    return label.astype(bool)


def ins_merger(annotations):
    tags_d = config.tags_d['ins']
    label = np.zeros(len(tags_d))
    for anno in annotations:
        for tag in anno.findall('tag'):
            name = tag.get('label')
            if name in tags_d:
                label[tags_d[name]] += 1

    label = (label / len(annotations)) >= .5
    return label[:6]  # ignore other & spurting bleeding


mergers = {
    'ana': ana_merger,
    'bus': bus_merger,
    'ins': ins_merger,
}


class MultiRaterAnnotation:
    def __init__(self, files: Tuple, mode: str, root: str = config.annotation_root):
        images = {}
        for file in files:
            xml = ET.parse(join(root, file)).getroot()
            for image in xml.findall('image'):
                name = image.get('name').split("/")[-1]
                if name not in images:
                    images[name] = []
                images[name].append(image)

        self.data = {}
        for name, annos in images.items():
            self.data[name] = mergers[mode](annos)


class TrainSet(Dataset):
    def __init__(self, mras: list, subset: str, width: int = WIDTH, height: int = HEIGHT, transform: nn.Module = None):
        if subset not in ['Equidistant', 'ActiveLearning/ana', 'ActiveLearning/bus', 'ActiveLearning/ins']:
            raise AttributeError("subset must be 'Equidistant' or 'ActiveLearning'!")

        self.transform = transform
        self.width = width
        self.height = height
        self._resize_compose = transforms.Compose([
            transforms.Resize(size=min(self.height, self.width)),
            transforms.CenterCrop(size=(self.height, self.width))
        ])

        self.frames = {
            frame: join(config.frame_root, round, subset, frame)
            for round in config.rounds
            for frame in listdir(join(config.frame_root, round, subset))
            if frame.endswith('.png')
        }

        self.labels = {}
        for mra in mras:
            self.labels.update(mra.data)

        self.data = sorted(list(set(self.frames.keys()) & set(self.labels.keys())))

        self.frame_cache = {}

    def _resize(self, frame: np.ndarray) -> np.ndarray:
        th = 5
        mask = frame.sum(axis=0) > th
        if mask.sum() > 0:  # if there are non black pixels crop black border
            coords = np.argwhere(mask)
            x0, y0 = coords.min(axis=0)
            x1, y1 = coords.max(axis=0) + 1
            frame = frame[:, x0:x1, y0:y1]

        return self._resize_compose(torch.tensor(frame)).numpy()

    def _load_frame(self, path) -> np.ndarray or torch.Tensor:
        # frame = np.asarray(
        #     Image.open(
        #         path
        #     ).convert('RGB'), dtype=np.uint8
        # ).transpose([2, 0, 1])
        #
        # if resize:
        #     frame = self._resize(frame)
        #
        # if transform and self.transform is not None:
        #     frame = self.transform(frame.transpose([1, 2, 0]))

        if path not in self.frame_cache:
            self.frame_cache[path] = cv2.resize(
                cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGB),
                (WIDTH, HEIGHT)
            )

        frame = self.frame_cache[path]

        if self.transform is not None:
            if isinstance(self.transform, A.Compose):
                transformed = self.transform(image=frame)
                frame = transformed['image']
            elif isinstance(self.transform, transforms.Compose):
                frame = self.transform(frame)
            else:
                raise TypeError("transform must be of type A.Compose, transforms.Compose or None!")

        return frame

    def __getitem__(self, item):
        n = self.data[item]
        return n, self._load_frame(self.frames[n]), self.labels[n]

    def __len__(self):
        return len(self.data)


class JoinedTrainset(Dataset):
    """
    Merges multiple TrainSets into one
    if no annotation available is will be filled with minus ones
    """

    def __init__(self, ana: List[TrainSet], bus: List[TrainSet], ins: List[TrainSet]):
        self.ana = ana
        self.bus = bus
        self.ins = ins

        datasets = []
        for ds in self.ana:
            datasets.append(set(ds.data))
        for ds in self.bus:
            datasets.append(set(ds.data))
        for ds in self.ins:
            datasets.append(set(ds.data))

        self.ana_map = {k: ds for ds in self.ana for k in ds.data}
        self.bus_map = {k: ds for ds in self.bus for k in ds.data}
        self.ins_map = {k: ds for ds in self.ins for k in ds.data}

        self.data = sorted(list(set.union(*datasets)))

    def pos_weight(self):
        ana = np.stack([self.ana_map[name].labels[name] for name in self.data if name in self.ana_map])
        bus = np.stack([self.bus_map[name].labels[name] for name in self.data if name in self.bus_map])
        ins = np.stack([self.ins_map[name].labels[name] for name in self.data if name in self.ins_map])
        ana_weight = np.sum(ana == 0, axis=0) / np.sum(ana == 1, axis=0)
        bus_weight = np.sum(bus == 0, axis=0) / np.sum(bus == 1, axis=0)
        ins_weight = np.sum(ins == 0, axis=0) / np.sum(ins == 1, axis=0)
        return np.concatenate([ana_weight, bus_weight, ins_weight])

    def __getitem__(self, item):
        name = self.data[item]
        ana_ds = self.ana_map.get(name, None)
        bus_ds = self.bus_map.get(name, None)
        ins_ds = self.ins_map.get(name, None)

        if name in self.ana_map:
            frame = ana_ds._load_frame(ana_ds.frames[name])
        elif name in self.bus_map:
            frame = bus_ds._load_frame(bus_ds.frames[name])
        elif name in self.ins_map:
            frame = ins_ds._load_frame(ins_ds.frames[name])
        else:
            raise KeyError(f"Frame {name} not found in any dataset!")

        labels = []

        if ana_ds is not None and name in ana_ds.labels:
            labels.append(ana_ds.labels[name].astype(np.uint8))
        else:
            labels.append(np.ones(2, dtype=np.uint8) * -1)

        if bus_ds is not None and name in bus_ds.labels:
            labels.append(bus_ds.labels[name].astype(np.uint8))
        else:
            labels.append(np.ones(9, dtype=np.uint8) * -1)

        if ins_ds is not None and name in ins_ds.labels:
            labels.append(ins_ds.labels[name].astype(np.uint8))
        else:
            labels.append(np.ones(6, dtype=np.uint8) * -1)

        label = np.concatenate(labels)

        return name, frame, label

    def __len__(self):
        return len(self.data)


class Frames(Dataset):
    def __init__(self, folder: str, width: int = WIDTH, height: int = HEIGHT, transform: nn.Module = None):
        self.transform = transform
        self.width = width
        self.height = height
        self._resize_compose = transforms.Compose([
            transforms.Resize(size=min(self.height, self.width)),
            transforms.CenterCrop(size=(self.height, self.width))
        ])

        self.frames = {
            frame: join(folder, frame)
            for frame in listdir(folder)
            if frame.endswith('.png')
        }

        self.data = sorted(self.frames.keys())

    def _resize(self, frame: np.ndarray) -> np.ndarray:
        th = 5
        mask = frame.sum(axis=0) > th
        if mask.sum() > 0:  # if there are non black pixels crop black border
            coords = np.argwhere(mask)
            x0, y0 = coords.min(axis=0)
            x1, y1 = coords.max(axis=0) + 1
            frame = frame[:, x0:x1, y0:y1]

        return self._resize_compose(torch.tensor(frame)).numpy()

    def _load_frame(self, path, resize: bool = True, transform: bool = True) -> np.ndarray or torch.Tensor:
        frame = np.asarray(
            Image.open(
                path
            ).convert('RGB'), dtype=np.uint8
        ).transpose([2, 0, 1])

        if resize:
            frame = self._resize(frame)

        if transform and self.transform is not None:
            frame = self.transform(frame.transpose([1, 2, 0]))

        return frame

    def __getitem__(self, item):
        name = self.data[item]
        frame = self._load_frame(self.frames[name])
        return name, frame

    def __len__(self):
        return len(self.data)


class Video(IterableDataset):
    def __init__(self, video: str,
                 mras: list = None, anonymization: str = None, blocked: list = None,
                 width: int = WIDTH, height: int = HEIGHT,
                 transform: nn.Module = None, resize: bool = True):
        self.video = video
        self.mras = mras
        self.anonymization = anonymization
        self.transform = transform
        self.width = width
        self.height = height
        self.resize = resize
        self._resize_compose = transforms.Compose([
            transforms.Resize(size=min(self.height, self.width)),
            transforms.CenterCrop(size=(self.height, self.width))
        ])
        self.name = video.split("/")[-1].split(".")[0]

        self.blocked = blocked if blocked is not None else []
        if mras is not None:
            for mra in mras:
                mra: MultiRaterAnnotation
                self.blocked.extend([int(k.split("_")[-1][:-4]) for k in mra.data.keys() if k.startswith(self.name)])

        if anonymization is not None:
            for line in open(anonymization, 'r').readlines():
                line = line.replace("\n", "")
                if len(line):
                    start, stop, *_ = line.split(" ")
                    self.blocked.extend(list(range(int(start), int(stop) + 1)))

        self.blocked = set(self.blocked)

    def _resize(self, frame: np.ndarray) -> np.ndarray:
        th = 5
        mask = frame.sum(axis=0) > th
        if mask.sum() > 0:  # if there are non black pixels crop black border
            coords = np.argwhere(mask)
            x0, y0 = coords.min(axis=0)
            x1, y1 = coords.max(axis=0) + 1
            frame = frame[:, x0:x1, y0:y1]

        return self._resize_compose(torch.tensor(frame)).numpy()

    def __iter__(self):
        cap = cv2.VideoCapture(self.video)
        n_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        idx = set(range(n_frames)) - self.blocked

        for i in sorted(idx):
            if int(cap.get(cv2.CAP_PROP_POS_FRAMES)) != i:
                cap.set(cv2.CAP_PROP_POS_FRAMES, i)

            ret, frame = cap.read()
            if not ret:
                break  # reached end of video

            frame = np.asarray(
                Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)).convert('RGB')
            ).transpose([2, 0, 1])

            if self.resize:
                frame = self._resize(frame)

            if self.transform is not None:
                frame = self.transform(frame.transpose([1, 2, 0]))

            yield self.name, i, frame
        cap.release()

    def __len__(self):
        cap = cv2.VideoCapture(self.video)
        n_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        idx = set(range(n_frames)) - self.blocked
        cap.release()
        return len(idx)


class ConcatIterableDataset(IterableDataset):
    def __init__(self, datasets):
        self.datasets = datasets

    def __iter__(self):
        for ds in self.datasets:
            for elem in ds:
                yield elem

    def __len__(self):
        return sum([len(ds) for ds in self.datasets])


if __name__ == '__main__':
    # from torch.utils.data import ConcatDataset
    #
    # for MODE in ['ana', 'bus', 'ins']:
    #     sts = TrainSet(
    #         height=HEIGHT, width=WIDTH,
    #         mras=[MultiRaterAnnotation(files=files, mode=MODE)
    #               for files in [f for f, r in config.annotations_per_round[MODE].items() if r == 0]],
    #         subset=f'Equidistant',
    #         transform=transform,
    #     )
    #
    #     annotations = [f for f, r in config.annotations_per_round[MODE].items() if r > 0]
    #     mras = [MultiRaterAnnotation(files=files, mode=MODE) for files in annotations]
    #     eq = TrainSet(
    #         height=HEIGHT, width=WIDTH,
    #         mras=mras,
    #         subset=f'Equidistant',
    #         transform=transform,
    #     )
    #     al = TrainSet(
    #         height=HEIGHT, width=WIDTH,
    #         mras=mras,
    #         subset=f'ActiveLearning/{MODE}',
    #         transform=transform,
    #     )
    #
    #     ds = ConcatDataset([sts, al, eq])
    #
    # for name, _, label in ds:
    #     print(name, ", ", ", ".join(label))
    #
    #

    from torch.utils.data import ConcatDataset
    MODE="ins"

    sts = TrainSet(
        height=HEIGHT, width=WIDTH,
        mras=[MultiRaterAnnotation(files=files, mode=MODE)
              for files in [f for f, r in config.annotations_per_round[MODE].items() if r == 0]],
        subset=f'Equidistant',
        transform=transform,
    )

    annotations = [f for f, r in config.annotations_per_round[MODE].items() if r > 0]
    mras = [MultiRaterAnnotation(files=files, mode=MODE) for files in annotations]
    # eq = TrainSet(
    #     height=HEIGHT, width=WIDTH,
    #     mras=mras,
    #     subset=f'Equidistant',
    #     transform=transform,
    # )
    #
    # ds_eq = ConcatDataset([sts, eq])
    # print("EQ")
    # hd = 0
    # dd = 0
    # for name, _, label in ds_eq:
    #     if "DD" in name:
    #         dd += label[config.tags_d[MODE]["Suction"]]
    #     else:
    #         hd += label[config.tags_d[MODE]["Suction"]]
    # print(hd, dd, len(ds_eq))


    al = TrainSet(
        height=HEIGHT, width=WIDTH,
        mras=mras,
        subset=f'ActiveLearning/{MODE}',
        transform=transform,
    )

    ds_al = ConcatDataset([sts, al])
    print("AL")
    hd = 0
    dd = 0
    for name, _, label in ds_al:
        if "DD" in name:
            dd += label[config.tags_d[MODE]["Suction"]]
        else:
            hd += label[config.tags_d[MODE]["Suction"]]
    print(hd, dd, len(ds_al))

    # # merge testset annoataions
    # for mode in ['ana', 'ins', 'bus']:
    #     len_d = {
    #         'ana':2, 'ins':6, 'bus':9
    #     }
    #     data = {}
    #     for files in config.test_annotations_d[mode]:
    #         data.update(MultiRaterAnnotation(files, mode, root=config.test_annotation_root).data)
    #
    #     with open(f"out/{mode}.csv", 'a') as out_f:
    #         print(f'frame,{",".join(sorted(config.tags_d[mode], key=lambda i :config.tags_d[mode][i])[:len_d[mode]])}', file=out_f)
    #         for k in sorted(data):
    #             print(f'{k},{",".join(list(data[k].astype(int).astype(str)))}', file=out_f)
    # exit()

    # for mode in ['ana', 'ins', 'bus']:
    #     mras = [MultiRaterAnnotation(files, mode, root=config.test_annotation_root) for files in config.test_annotations_d[mode]]
    #     ds = TrainSet(mras, 'Equidistant', transform=None)
    #     print(mode, np.stack(ds.labels.values()).sum(axis=0), len(ds.labels),  np.stack(ds.labels.values()).sum(axis=0)/len(ds.labels))
    # input()

    # for files in config.annotations_d['bus']:
    #     print(files)
    #     sys.stdout.flush()
    #     MultiRaterAnnotation(files, 'bus')
    #     time.sleep(1)
    #
    # logging.disable(logging.WARNING)
    # # mras = [MultiRaterAnnotation(files, "bus") for files in config.annotations_d["bus"]]
    # # ds = TrainSet(mras, "Equidistant", transform=None)
    # # print([k for k in ds.data if ds.labels[k][7]])
    #
    # print(config.rounds)
    # for subset in ['Equidistant', 'ActiveLearning']:
    #     for mode in ['ana', 'ins', 'bus']:
    #         mras = [MultiRaterAnnotation(files, mode) for files in config.annotations_d[mode]]
    #         ds = TrainSet(mras, subset if subset == 'Equidistant' else f"{subset}/{mode}", transform=None)
    #         print(subset, mode, len(ds), np.stack([ds.labels[k] for k in ds.data]).sum(axis=0))

    # v = Video(
    #     video="/path/to/data/Videos/DD/raw/DD_01.mp4",
    #     anonymization="/path/to/data/Videos/DD/anonymization/DD_01_segments.txt",
    #     resize=False,
    # )
    # print(len(v), len(v.blocked))
