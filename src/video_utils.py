import os.path
import subprocess
import typing as T
from os.path import join

import cv2
import numpy as np
import torchvision.transforms
from PIL import Image
from cv2 import VideoCapture, CAP_PROP_POS_FRAMES, cvtColor, COLOR_BGR2RGB, CAP_PROP_FPS, CAP_PROP_FRAME_COUNT
from torchvision.utils import save_image
from tqdm import tqdm


def extract_frames(idx: T.Iterable, video: str, out: str, width: int = None, height: int = None, verbose=False):
    """
    Extracts frames from a video and saves them as png files.
    :param idx: frame indices to extract
    :param video:  path to video
    :param out:  path to output directory
    :param width:  resize width
    :param height:  resize height
    :param verbose:  show progress bar
    :return:  None
    """
    cap = VideoCapture(video)
    video_name = ".".join(video.split("/")[-1].split(".")[:-1])

    if height is None or width is None:
        resize = torchvision.transforms.ToTensor()  # do not resize
    else:
        resize = torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Resize(size=min(height, width)),
            torchvision.transforms.CenterCrop(size=(height, width))
        ])

    w = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    h = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    if verbose: bar = tqdm(total=len(idx), desc='Extracting')

    for i in idx:
        cap.set(CAP_PROP_POS_FRAMES, i)
        ret, frame = cap.read()

        if w < h:
            # if w < h the frames need to be stretched along the x axis (probably mono frame of stereo video?)
            frame = cv2.resize(frame, (int(w * 2), int(h)))

        if not ret:
            raise ValueError(f"Frame ({i}) can not be retrieved from video ({video})!")
        frame = resize(Image.fromarray(cvtColor(frame, COLOR_BGR2RGB)).convert('RGB'))
        save_image(tensor=frame, fp=join(out, f"{video_name}_{str(i).zfill(8)}.png"))

        if verbose: bar.update()

    cap.release()
    if verbose: bar.close()


def get_videometa(video):
    """
    Returns meta information about a video.
    """
    cap = VideoCapture(video)
    fps = cap.get(CAP_PROP_FPS)
    n_frames = cap.get(CAP_PROP_FRAME_COUNT)
    cap.release()
    return {
        'fps': fps,
        'n_frames': n_frames,
    }


def extract_equidistant(fps: float, video: str, out: str, anno: str = None, width: int = None, height: int = None,
                        verbose=False):
    """
    Extracts frames from a video in an equidistant manner and saves them as png files.
    :param fps: sampling rate in frames per second
    :param video:  path to video
    :param out:  path to output directory
    :param anno:  path to annotation file listing frames to ignore
    :param width:  resize width
    :param height:  resize height
    :param verbose:  show progress bar
    :return:  None
    """
    meta = get_videometa(video)
    assert fps <= meta['fps']

    idx = [np.round(i).astype(int) for i in np.arange(start=0, stop=meta['n_frames'], step=meta['fps'] / fps)]
    if anno is not None:
        blocked = []
        for line in open(anno, 'r').readlines():
            line = line.replace("\n", "")
            if len(line):
                start, stop, *_ = line.split(" ")
                blocked.extend(list(range(int(start), int(stop) + 1)))
        idx = [i for i in idx if i not in blocked]

    extract_frames(idx, video=video, out=out, width=width, height=height, verbose=verbose)


def extract_amount(n_frames: int, video: str, out: str,
                   start: int = 0, endpoint: bool = False,
                   width: int = None, height: int = None,
                   verbose=False):
    """
    Extracts a given amount of frames from a video and saves them as png files.
    :param n_frames: number of frames to extract
    :param video:  path to video
    :param out:  path to output directory
    :param start:  start frame
    :param endpoint:  include endpoint
    :param width:  resize width
    :param height:  resize height
    :param verbose:  show progress bar
    :return:  None
    """
    meta = get_videometa(video)
    assert n_frames - start <= meta['n_frames']
    idx = np.linspace(num=n_frames, start=start, stop=meta['n_frames'], endpoint=endpoint, dtype=int)
    extract_frames(idx, video=video, out=out, width=width, height=height, verbose=verbose)


def ranges(nums):
    nums = sorted(set(nums))
    gaps = [[s, e] for s, e in zip(nums, nums[1:]) if s + 1 < e]
    edges = iter(nums[:1] + sum(gaps, []) + nums[-1:])
    return list(zip(edges, edges))


def scan4white(video, out_file):
    """
    Scans a video for white frames and writes the ranges to a file.
    This creates anonymization annotation files from an already anonymized video.
    :param video:  path to video
    :param out_file:  path to output file
    :return:  None
    """
    cap = cv2.VideoCapture(video)
    n_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    idx = []
    bar = tqdm(total=n_frames, desc=f"{len(idx)} removed Frames")
    while 1:
        ret, frame = cap.read()
        if not ret:
            break
        # print(frame[0, 0, 0], " ", frame.min())
        if frame[0, 0, 0] >= 250 and frame.min() >= 250:
            idx.append(int(cap.get(cv2.CAP_PROP_POS_FRAMES)))
            bar.desc = f"{len(idx)} removed Frames"
        bar.update()

    cap.release()

    with open(out_file, 'a') as f:
        for (s, e) in ranges(idx):
            print(f"{s} {e} white", file=f)


def concat_videos(videos: list, name, out="/path/to/data/Videos/DD/raw/"):
    """
    Concatenates videos.
    :param videos:  list of paths to videos
    :param name:  name of the concatenated video
    :param out:  path to output directory
    :return: 
    """
    ts_list = []
    for i, video in enumerate(videos):
        ts = os.path.join(out, f'{name}_{i}.ts')
        ts_list.append(ts)
        cmd = f"ffmpeg -i '{video}' -c copy -bsf:v h264_mp4toannexb -f mpegts {ts}"
        print(cmd)
        os.system(cmd)

    os.system(
        f"ffmpeg -i  'concat:{'|'.join(ts_list)}' -c copy -bsf:a aac_adtstoasc {os.path.join(out, f'{name}.mp4')}")

    for ts in ts_list:
        os.system(f"rm {ts}")


if __name__ == '__main__':

    # analyze video for ananymized frames
    scan4white("/path/to/data/Videos/small/HD/DaVinciOesi12_anno.MOV",
               "/path/to/data/Videos/HD/ano/DaVinciOesi12_anno.MOV.txt")


    # extract equidistant frames from videos
    for (o, v, a) in [
        # ("AL01/Equidistant", "DD/DD_01.mp4"),
        # ("AL01/Equidistant", "HD/DaVinciOesi2_ano.mp4"),
        # ("AL02/Equidistant", "DD/DD_02.mp4"),
        # ("AL02/Equidistant", "HD/DaVinciOesi3_ano.mp4"),
        # ("AL03/Equidistant", "DD/DD_03.mp4"),
        # ("AL03/Equidistant", "HD/DaVinciOesi5_ano.mp4"),
        # ("AL04/Equidistant", "DD/DD_04.mp4"),
        # ("AL04/Equidistant", "HD/DaVinciOesi6_anno.MOV"),
        # ("AL08/Equidistant", "HD/DaVinciOesi7_anno.MOV"),
        # ("AL09/Equidistant", "HD/DaVinciOesi8_anno.MOV"),
        # ("AL05/Equidistant", "DD/raw/DD_05.mp4", "DD/ano/DD_05_segments.txt"),
        # ("AL06/Equidistant", "DD/raw/DD_06.mp4", "DD/ano/DD_06_segments.txt"),
        # ("AL07/Equidistant", "DD/raw/DD_07.mp4", "DD/ano/DD_07_segments.txt"),
        # ("AL08/Equidistant", "DD/raw/DD_08.mp4", "DD/ano/DD_08_segments.txt"),
        # ("AL07/Equidistant", "HD/DaVinciOesi11_anno.MOV", "HD/ano/DaVinciOesi11_anno.MOV.txt"),
        # ("AL10/Equidistant", "DD/raw/DD_10.mp4", "DD/ano/DD_10_segments.txt"),
        # ("AL09/Equidistant", "DD/raw/DD_09.mp4", "DD/ano/DD_09_segments.txt"),
        ("AL09/Equidistant", "HD/DaVinciOesi12_anno.MOV", "HD/ano/DaVinciOesi12_anno.MOV.txt"),
    ]:
        extract_equidistant(
            fps=1 / 120,
            video=f"/path/to/data/Videos/{v}",
            out=f"/path/to/data/Frames/{o}/",
            anno=f"/path/to/data/Videos/{a}",
            verbose=True,
        )
