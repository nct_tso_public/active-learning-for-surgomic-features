# Models
This folder contains the trained models.

## Structure
Each folder contains one trained Model.
The folder name is the name of the model.
The folder contains the following files:
- `config.py`: The configuration used to train the model.
- `frames.txt`: The frames used to train the model.
- `Model.pt`: The state dict of the model.
- `train_*.py`: The training script used to train the model.

## Naming convention
The naming of the models is as follows:
- `AL10`: These models were trained on the data selected by active learning after the 10th round. All videos were available for training.
- `EQ10`: These models were trained on the data selected by equal sampling after the 10th round. All videos were available for training.
- `ALL`: These models were trained on both the data selected by active learning and equal sampling after the 10th round. All videos were available for training.
- `LARGE`: These models were trained on the data according to the trailing naming convention but with a larger image resolution.
- `DET`: These models were trained on all available data disabling the bayesian model architecture to get deterministic predictions.

The best performing models in the most cases are `LARGE_ALL_*`.