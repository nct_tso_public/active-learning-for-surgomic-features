annotation_root = "/mnt/ceph/tco/TCO-Staff/Projects/SurgOmics_Prospective_AL/Annotations/Train/"
frame_root = "/mnt/ceph/tco/TCO-Staff/Projects/SurgOmics_Prospective_AL/Frames/"
video_root = "/mnt/ceph/tco/TCO-Staff/Projects/SurgOmics_Prospective_AL/Videos"
model_root = "/mnt/ceph/tco/TCO-Staff/Projects/SurgOmics_Prospective_AL/Models"

# General #
tags_d = {
    'ana': {
        'Azygos Vein': 0,
        'Gastric Tube': 1,
        'Spurting Bleeding': 2,
    },
    'bus': {
        'Blood: 0-No': 0,
        'Blood: 1-Small': 1,
        'Blood: 2-Accumulation': 2,
        'Blood: 3-Great': 3,
        'Blood: 4-InterventionRequired': 4,
        'Smoke: 0-No': 5,
        'Smoke: 1-Small': 6,
        'Smoke: 2-Increased': 7,
        'Smoke: 3-NoVisibility': 8,
        'Spurting Bleeding': 9,
    },
    'ins': {
        'Vessel sealer': 0,
        'Permanent cautery hook': 1,
        'Clip applier (metal)': 2,
        'Large clip applier (Hem-O-Lok)': 3,
        'Monopolar curved scissors': 4,
        'Suction': 5,
        'Other': 6,
        'Spurting Bleeding': 7,
    }
}

# Model Training #
rounds = [
    'AL00',
    'AL01',
    'AL02',
    'AL03',
    'AL04',
    'AL05',
    'AL06',
    'AL07',
    'AL08',
    'AL09',
    'AL10'
]

# later annotations overwrite earlier ones!
annotations_d = {
    "ana": [
        ("Anatomy_Startset_0.xml", "Anatomy_Startset_1.xml", "Anatomy_Startset_2.xml"),  # startset
        ("Ana_01_Equidistant_0.xml", "Ana_01_Equidistant_1.xml", "Ana_01_Equidistant_2.xml"),  # R01 EQ
        ("Ana_AL01_AL_0.xml", "Ana_AL01_AL_1.xml", "Ana_AL01_AL_2.xml"),  # R01 AL
        ("Ana_02_EQ_0.xml", "Ana_02_EQ_1.xml", "Ana_02_EQ_2.xml"),  # R02 EQ
        ("Ana_02_AL_0.xml", "Ana_02_AL_1.xml", "Ana_02_AL_2.xml"),  # R02 AL
        ("Ana_03_EQ_0.xml", "Ana_03_EQ_1.xml", "Ana_03_EQ_2.xml"),  # R03 EQ
        ("Ana_03_AL_0.xml", "Ana_03_AL_1.xml", "Ana_03_AL_2.xml"),  # R03 AL
        ("Ana_04_EQ_0.xml", "Ana_04_EQ_1.xml", "Ana_04_EQ_2.xml"),  # R04 EQ
        ("Ana_04_AL_0.xml", "Ana_04_AL_1.xml", "Ana_04_AL_2.xml"),  # R04 AL
        ("Ana_05_EQ_0.xml", "Ana_05_EQ_1.xml", "Ana_05_EQ_2.xml"),  # R05 EQ
        ("Ana_05_AL_0.xml", "Ana_05_AL_1.xml", "Ana_05_AL_2.xml"),  # R05 AL
        ("Ana_06_EQ_0.xml", "Ana_06_EQ_1.xml", "Ana_06_EQ_2.xml"),  # R06 EQ
        ("Ana_06_AL_0.xml", "Ana_06_AL_1.xml", "Ana_06_AL_2.xml"),  # R06 AL
        ("Ana_07_EQ_0.xml", "Ana_07_EQ_1.xml", "Ana_07_EQ_2.xml"),  # R07 EQ
        ("Ana_07_AL_0.xml", "Ana_07_AL_1.xml", "Ana_07_AL_2.xml"),  # R07 AL
        ("Ana_08_EQ_0.xml", "Ana_08_EQ_1.xml", "Ana_08_EQ_2.xml"),  # R08 EQ
        ("Ana_08_AL_0.xml", "Ana_08_AL_1.xml", "Ana_08_AL_2.xml"),  # R08 AL
        ("Ana_09_EQ_0.xml", "Ana_09_EQ_1.xml", "Ana_09_EQ_2.xml"),  # R09 EQ
        ("Ana_09_AL_0.xml", "Ana_09_AL_1.xml", "Ana_09_AL_2.xml"),  # R09 AL
        ("Ana_10_EQ_0.xml", "Ana_10_EQ_1.xml", "Ana_10_EQ_2.xml"),  # R10 EQ
        ("Ana_10_AL_0.xml", "Ana_10_AL_1.xml", "Ana_10_AL_2.xml"),  # R10 AL
    ],
    "bus": [
        ("BloodAndSmoke_Startset_0.xml", "BloodAndSmoke_Startset_1.xml", "BloodAndSmoke_Startset_2.xml"),  # startset
        ("BloodAndSmoke_Startset_Diskussion.xml",),  # startset
        ("BloodAndSmoke_Startset_Extension.xml",),  # startset
        ("BuS_01_Equidistant_0.xml", "BuS_01_Equidistant_1.xml", "BuS_01_Equidistant_2.xml"),  # R01 EQ
        ("BuS_AL01_AL_0.xml", "BuS_AL01_AL_1.xml", "BuS_AL01_AL_2.xml"),  # R01 AL
        ("BuS_AL01_Discussion.xml",),  # R01
        ("BuS_02_EQ_0.xml", "BuS_02_EQ_1.xml", "BuS_02_EQ_2.xml"),  # R02 EQ
        ("BuS_02_AL_0.xml", "BuS_02_AL_1.xml", "BuS_02_AL_2.xml"),  # R02 AL
        ("BuS_02_Discussion.xml",),  # R02
        ("BuS_03_EQ_0.xml", "BuS_03_EQ_1.xml", "BuS_03_EQ_2.xml"),  # R03 EQ
        ("BuS_03_AL_0.xml", "BuS_03_AL_1.xml", "BuS_03_AL_2.xml"),  # R03 AL
        ("BuS_03_Discussion.xml",),  # R03
        ("BuS_04_EQ_0.xml", "BuS_04_EQ_1.xml", "BuS_04_EQ_2.xml"),  # R04 EQ
        ("BuS_04_AL_0.xml", "BuS_04_AL_1.xml", "BuS_04_AL_2.xml"),  # R04 AL
        ("BuS_04_Discussion.xml",),  # R04
        ("BuS_05_EQ_0.xml", "BuS_05_EQ_1.xml", "BuS_05_EQ_2.xml"),  # R05 EQ
        ("BuS_05_AL_0.xml", "BuS_05_AL_1.xml", "BuS_05_AL_2.xml"),  # R05 AL
        ("BuS_05_Discussion.xml",),  # R05
        ("BuS_06_EQ_0.xml", "BuS_06_EQ_1.xml", "BuS_06_EQ_2.xml"),  # R06 EQ
        ("BuS_06_AL_0.xml", "BuS_06_AL_1.xml", "BuS_06_AL_2.xml"),  # R06 AL
        ("BuS_06_Discussion.xml",),  # R06
        ("BuS_07_EQ_0.xml", "BuS_07_EQ_1.xml", "BuS_07_EQ_2.xml"),  # R07 EQ
        ("BuS_07_AL_0.xml", "BuS_07_AL_1.xml", "BuS_07_AL_2.xml"),  # R07 AL
        ("BuS_07_Discussion.xml",),  # R07
        ("BuS_08_EQ_0.xml", "BuS_08_EQ_1.xml", "BuS_08_EQ_2.xml"),  # R08 EQ
        ("BuS_08_AL_0.xml", "BuS_08_AL_1.xml", "BuS_08_AL_2.xml"),  # R08 AL
        ("BuS_08_Discussion.xml",),  # R08
        ("BuS_09_EQ_0.xml", "BuS_09_EQ_1.xml", "BuS_09_EQ_2.xml"),  # R09 EQ
        ("BuS_09_AL_0.xml", "BuS_09_AL_1.xml", "BuS_09_AL_2.xml"),  # R09 AL
        ("BuS_09_Discussion.xml",),  # R09
        ("BuS_10_EQ_0.xml", "BuS_10_EQ_1.xml", "BuS_10_EQ_2.xml"),  # R10 EQ
        ("BuS_10_AL_0.xml", "BuS_10_AL_1.xml", "BuS_10_AL_2.xml"),  # R10 AL
        ("BuS_10_Discussion.xml",),  # R10

    ],
    'ins': [
        ("Instruments_Startset_0.xml", "Instruments_Startset_1.xml", "Instruments_Startset_2.xml"),  # startset
        ("Ins_01_Equidistant_0.xml", "Ins_01_Equidistant_1.xml", "Ins_01_Equidistant_2.xml"),  # R01 EQ
        ("Ins_AL01_AL_0.xml", "Ins_AL01_AL_1.xml", "Ins_AL01_AL_2.xml"),  # R01 AL
        ("Ins_02_EQ_0.xml", "Ins_02_EQ_1.xml", "Ins_02_EQ_2.xml"),  # R02 EQ
        ("Ins_02_AL_0.xml", "Ins_02_AL_1.xml", "Ins_02_AL_2.xml"),  # R02 AL
        ("Ins_03_EQ_0.xml", "Ins_03_EQ_1.xml", "Ins_03_EQ_2.xml"),  # R03 EQ
        ("Ins_03_AL_0.xml", "Ins_03_AL_1.xml", "Ins_03_AL_2.xml"),  # R03 AL
        ("Ins_04_EQ_0.xml", "Ins_04_EQ_1.xml", "Ins_04_EQ_2.xml"),  # R04 EQ
        ("Ins_04_AL_0.xml", "Ins_04_AL_1.xml", "Ins_04_AL_2.xml"),  # R04 AL
        ("Ins_05_EQ_0.xml", "Ins_05_EQ_1.xml", "Ins_05_EQ_2.xml"),  # R05 EQ
        ("Ins_05_AL_0.xml", "Ins_05_AL_1.xml", "Ins_05_AL_2.xml"),  # R05 AL
        ("Ins_06_EQ_0.xml", "Ins_06_EQ_1.xml", "Ins_06_EQ_2.xml"),  # R06 EQ
        ("Ins_06_AL_0.xml", "Ins_06_AL_1.xml", "Ins_06_AL_2.xml"),  # R06 AL
        ("Ins_07_EQ_0.xml", "Ins_07_EQ_1.xml", "Ins_07_EQ_2.xml"),  # R07 EQ
        ("Ins_07_AL_0.xml", "Ins_07_AL_1.xml", "Ins_07_AL_2.xml"),  # R07 AL
        ("Ins_08_EQ_0.xml", "Ins_08_EQ_1.xml", "Ins_08_EQ_2.xml"),  # R08 EQ
        ("Ins_08_AL_0.xml", "Ins_08_AL_1.xml", "Ins_08_AL_2.xml"),  # R08 AL
        ("Ins_09_EQ_0.xml", "Ins_09_EQ_1.xml", "Ins_09_EQ_2.xml"),  # R09 EQ
        ("Ins_09_AL_0.xml", "Ins_09_AL_1.xml", "Ins_09_AL_2.xml"),  # R09 AL
        ("Ins_10_EQ_0.xml", "Ins_10_EQ_1.xml", "Ins_10_EQ_2.xml"),  # R10 EQ
        ("Ins_10_AL_0.xml", "Ins_10_AL_1.xml", "Ins_10_AL_2.xml"),  # R10 AL
    ]
}

annotations_per_round = {
    "ana": {
        ("Anatomy_Startset_0.xml", "Anatomy_Startset_1.xml", "Anatomy_Startset_2.xml"): 0,  # startset
        ("Ana_01_Equidistant_0.xml", "Ana_01_Equidistant_1.xml", "Ana_01_Equidistant_2.xml"): 1,  # R01 EQ
        ("Ana_AL01_AL_0.xml", "Ana_AL01_AL_1.xml", "Ana_AL01_AL_2.xml"): 1,  # R01 AL
        ("Ana_02_EQ_0.xml", "Ana_02_EQ_1.xml", "Ana_02_EQ_2.xml"): 2,  # R02 EQ
        ("Ana_02_AL_0.xml", "Ana_02_AL_1.xml", "Ana_02_AL_2.xml"): 2,  # R02 AL
        ("Ana_03_EQ_0.xml", "Ana_03_EQ_1.xml", "Ana_03_EQ_2.xml"): 3,  # R03 EQ
        ("Ana_03_AL_0.xml", "Ana_03_AL_1.xml", "Ana_03_AL_2.xml"): 3,  # R03 AL
        ("Ana_04_EQ_0.xml", "Ana_04_EQ_1.xml", "Ana_04_EQ_2.xml"): 4,  # R04 EQ
        ("Ana_04_AL_0.xml", "Ana_04_AL_1.xml", "Ana_04_AL_2.xml"): 4,  # R04 AL
        ("Ana_05_EQ_0.xml", "Ana_05_EQ_1.xml", "Ana_05_EQ_2.xml"): 5,  # R05 EQ
        ("Ana_05_AL_0.xml", "Ana_05_AL_1.xml", "Ana_05_AL_2.xml"): 5,  # R05 AL
        ("Ana_06_EQ_0.xml", "Ana_06_EQ_1.xml", "Ana_06_EQ_2.xml"): 6,  # R06 EQ
        ("Ana_06_AL_0.xml", "Ana_06_AL_1.xml", "Ana_06_AL_2.xml"): 6,  # R06 AL
        ("Ana_07_EQ_0.xml", "Ana_07_EQ_1.xml", "Ana_07_EQ_2.xml"): 7,  # R07 EQ
        ("Ana_07_AL_0.xml", "Ana_07_AL_1.xml", "Ana_07_AL_2.xml"): 7,  # R07 AL
        ("Ana_08_EQ_0.xml", "Ana_08_EQ_1.xml", "Ana_08_EQ_2.xml"): 8,  # R08 EQ
        ("Ana_08_AL_0.xml", "Ana_08_AL_1.xml", "Ana_08_AL_2.xml"): 8,  # R08 AL
        ("Ana_09_EQ_0.xml", "Ana_09_EQ_1.xml", "Ana_09_EQ_2.xml"): 9,  # R09 EQ
        ("Ana_09_AL_0.xml", "Ana_09_AL_1.xml", "Ana_09_AL_2.xml"): 9,  # R09 AL
        ("Ana_10_EQ_0.xml", "Ana_10_EQ_1.xml", "Ana_10_EQ_2.xml"): 10,  # R10 EQ
        ("Ana_10_AL_0.xml", "Ana_10_AL_1.xml", "Ana_10_AL_2.xml"): 10,  # R10 AL
    },
    "bus": {
        ("BloodAndSmoke_Startset_0.xml", "BloodAndSmoke_Startset_1.xml", "BloodAndSmoke_Startset_2.xml"): 0,  # startset
        ("BloodAndSmoke_Startset_Diskussion.xml",): 0,  # startset
        ("BloodAndSmoke_Startset_Extension.xml",): 0,  # startset
        ("BuS_01_Equidistant_0.xml", "BuS_01_Equidistant_1.xml", "BuS_01_Equidistant_2.xml"): 1,  # R01 EQ
        ("BuS_AL01_AL_0.xml", "BuS_AL01_AL_1.xml", "BuS_AL01_AL_2.xml"): 1,  # R01 AL
        ("BuS_AL01_Discussion.xml",): 1,  # R01
        ("BuS_02_EQ_0.xml", "BuS_02_EQ_1.xml", "BuS_02_EQ_2.xml"): 2,  # R02 EQ
        ("BuS_02_AL_0.xml", "BuS_02_AL_1.xml", "BuS_02_AL_2.xml"): 2,  # R02 AL
        ("BuS_02_Discussion.xml",): 2,  # R02
        ("BuS_03_EQ_0.xml", "BuS_03_EQ_1.xml", "BuS_03_EQ_2.xml"): 3,  # R03 EQ
        ("BuS_03_AL_0.xml", "BuS_03_AL_1.xml", "BuS_03_AL_2.xml"): 3,  # R03 AL
        ("BuS_03_Discussion.xml",): 3,  # R03
        ("BuS_04_EQ_0.xml", "BuS_04_EQ_1.xml", "BuS_04_EQ_2.xml"): 4,  # R04 EQ
        ("BuS_04_AL_0.xml", "BuS_04_AL_1.xml", "BuS_04_AL_2.xml"): 4,  # R04 AL
        ("BuS_04_Discussion.xml",): 4,  # R04
        ("BuS_05_EQ_0.xml", "BuS_05_EQ_1.xml", "BuS_05_EQ_2.xml"): 5,  # R05 EQ
        ("BuS_05_AL_0.xml", "BuS_05_AL_1.xml", "BuS_05_AL_2.xml"): 5,  # R05 AL
        ("BuS_05_Discussion.xml",): 5,  # R05
        ("BuS_06_EQ_0.xml", "BuS_06_EQ_1.xml", "BuS_06_EQ_2.xml"): 6,  # R06 EQ
        ("BuS_06_AL_0.xml", "BuS_06_AL_1.xml", "BuS_06_AL_2.xml"): 6,  # R06 AL
        ("BuS_06_Discussion.xml",): 6,  # R06
        ("BuS_07_EQ_0.xml", "BuS_07_EQ_1.xml", "BuS_07_EQ_2.xml"): 7,  # R07 EQ
        ("BuS_07_AL_0.xml", "BuS_07_AL_1.xml", "BuS_07_AL_2.xml"): 7,  # R07 AL
        ("BuS_07_Discussion.xml",): 7,  # R07
        ("BuS_08_EQ_0.xml", "BuS_08_EQ_1.xml", "BuS_08_EQ_2.xml"): 8,  # R08 EQ
        ("BuS_08_AL_0.xml", "BuS_08_AL_1.xml", "BuS_08_AL_2.xml"): 8,  # R08 AL
        ("BuS_08_Discussion.xml",): 8,  # R08
        ("BuS_09_EQ_0.xml", "BuS_09_EQ_1.xml", "BuS_09_EQ_2.xml"): 9,  # R09 EQ
        ("BuS_09_AL_0.xml", "BuS_09_AL_1.xml", "BuS_09_AL_2.xml"): 9,  # R09 AL
        ("BuS_09_Discussion.xml",): 9,  # R09
        ("BuS_10_EQ_0.xml", "BuS_10_EQ_1.xml", "BuS_10_EQ_2.xml"): 10,  # R10 EQ
        ("BuS_10_AL_0.xml", "BuS_10_AL_1.xml", "BuS_10_AL_2.xml"): 10,  # R10 AL
        ("BuS_10_Discussion.xml",): 10,  # R10
    },
    'ins': {
        ("Instruments_Startset_0.xml", "Instruments_Startset_1.xml", "Instruments_Startset_2.xml"): 0,  # startset
        ("Ins_01_Equidistant_0.xml", "Ins_01_Equidistant_1.xml", "Ins_01_Equidistant_2.xml"): 1,  # R01 EQ
        ("Ins_AL01_AL_0.xml", "Ins_AL01_AL_1.xml", "Ins_AL01_AL_2.xml"): 1,  # R01 AL
        ("Ins_02_EQ_0.xml", "Ins_02_EQ_1.xml", "Ins_02_EQ_2.xml"): 2,  # R02 EQ
        ("Ins_02_AL_0.xml", "Ins_02_AL_1.xml", "Ins_02_AL_2.xml"): 2,  # R02 AL
        ("Ins_03_EQ_0.xml", "Ins_03_EQ_1.xml", "Ins_03_EQ_2.xml"): 3,  # R03 EQ
        ("Ins_03_AL_0.xml", "Ins_03_AL_1.xml", "Ins_03_AL_2.xml"): 3,  # R03 AL
        ("Ins_04_EQ_0.xml", "Ins_04_EQ_1.xml", "Ins_04_EQ_2.xml"): 4,  # R04 EQ
        ("Ins_04_AL_0.xml", "Ins_04_AL_1.xml", "Ins_04_AL_2.xml"): 4,  # R04 AL
        ("Ins_05_EQ_0.xml", "Ins_05_EQ_1.xml", "Ins_05_EQ_2.xml"): 5,  # R05 EQ
        ("Ins_05_AL_0.xml", "Ins_05_AL_1.xml", "Ins_05_AL_2.xml"): 5,  # R05 AL
        ("Ins_06_EQ_0.xml", "Ins_06_EQ_1.xml", "Ins_06_EQ_2.xml"): 6,  # R06 EQ
        ("Ins_06_AL_0.xml", "Ins_06_AL_1.xml", "Ins_06_AL_2.xml"): 6,  # R06 AL
        ("Ins_07_EQ_0.xml", "Ins_07_EQ_1.xml", "Ins_07_EQ_2.xml"): 7,  # R07 EQ
        ("Ins_07_AL_0.xml", "Ins_07_AL_1.xml", "Ins_07_AL_2.xml"): 7,  # R07 AL
        ("Ins_08_EQ_0.xml", "Ins_08_EQ_1.xml", "Ins_08_EQ_2.xml"): 8,  # R08 EQ
        ("Ins_08_AL_0.xml", "Ins_08_AL_1.xml", "Ins_08_AL_2.xml"): 8,  # R08 AL
        ("Ins_09_EQ_0.xml", "Ins_09_EQ_1.xml", "Ins_09_EQ_2.xml"): 9,  # R09 EQ
        ("Ins_09_AL_0.xml", "Ins_09_AL_1.xml", "Ins_09_AL_2.xml"): 9,  # R09 AL
        ("Ins_10_EQ_0.xml", "Ins_10_EQ_1.xml", "Ins_10_EQ_2.xml"): 10,  # R10 EQ
        ("Ins_10_AL_0.xml", "Ins_10_AL_1.xml", "Ins_10_AL_2.xml"): 10,  # R10 AL
    }
}

# Frame Selection #
videos = [
    ("HD/DaVinciOesi1_ano.mp4", "HD/ano/DaVinciOesi1_ano.mp4.txt"),  # startset
    ("HD/DaVinciOesi2_ano.mp4", "HD/ano/DaVinciOesi2_ano.mp4.txt"),  # AL1
    ("HD/DaVinciOesi3_ano.mp4", "HD/ano/DaVinciOesi3_ano.mp4.txt"),  # AL2
    ("HD/DaVinciOesi5_ano.mp4", "HD/ano/DaVinciOesi5_ano.mp4.txt"),  # AL3
    ("HD/DaVinciOesi6_anno.MOV", "HD/ano/DaVinciOesi6_anno.MOV.txt"),  # AL4
    ("HD/DaVinciOesi9_anno.MOV", "HD/ano/DaVinciOesi9_anno.MOV.txt"),  # AL5
    ("HD/DaVinciOesi10_anno.mp4", "HD/ano/DaVinciOesi10_anno.MOV.txt"),  # AL6
    ("HD/DaVinciOesi11_anno.mp4", "HD/ano/DaVinciOesi11_anno.MOV.txt"),  # AL7
    ("HD/DaVinciOesi7_anno.MOV", "HD/ano/DaVinciOesi7_anno.MOV.txt"),  # AL8
    ("HD/DaVinciOesi12_anno.MOV", "HD/ano/DaVinciOesi12_anno.MOV.txt"),  # AL9
    ("HD/DaVinciOesi8_anno.MOV", "HD/ano/DaVinciOesi8_anno.MOV.txt"),  # AL10

    ("DD/raw/DD_00.mp4", "DD/ano/DD_00.txt"),  # startset
    ("DD/raw/DD_01.mp4", "DD/ano/DD_01_segments.txt"),  # AL1
    ("DD/raw/DD_02.mp4", "DD/ano/DD_02_segments.txt"),  # AL2
    ("DD/raw/DD_03.mp4", "DD/ano/DD_03_segments.txt"),  # AL3
    ("DD/raw/DD_04.mp4", "DD/ano/DD_04_segments.txt"),  # AL4
    ("DD/raw/DD_05.mp4", "DD/ano/DD_05_segments.txt"),  # AL5
    ("DD/raw/DD_06.mp4", "DD/ano/DD_06_segments.txt"),  # AL6
    ("DD/raw/DD_07.mp4", "DD/ano/DD_07_segments.txt"),  # AL7
    ("DD/raw/DD_08.mp4", "DD/ano/DD_08_segments.txt"),  # AL8
    ("DD/raw/DD_09.mp4", "DD/ano/DD_09_segments.txt"),  # AL9
    ("DD/raw/DD_10.mp4", "DD/ano/DD_10_segments.txt"),  # AL10
]

# Test #
test_annotations_d = {
    "ana": [
        ("Anatomy_Testset_DD_1.xml", "Anatomy_Testset_DD_2.xml", "Anatomy_Testset_DD_4.xml",
         "Anatomy_Testset_DD_5.xml", "Anatomy_Testset_DD_6.xml", "Anatomy_Testset_DD_7.xml"),
        ("Anatomy_Testset_HD_1.xml", "Anatomy_Testset_HD_2.xml", "Anatomy_Testset_HD_4.xml",
         "Anatomy_Testset_HD_5.xml", "Anatomy_Testset_HD_6.xml", "Anatomy_Testset_HD_7.2.xml"),

    ],
    "bus": [
        ("BloodAndSmoke_Testset_DD_1.xml", "BloodAndSmoke_Testset_DD_2.xml", "BloodAndSmoke_Testset_DD_4.xml",
         "BloodAndSmoke_Testset_DD_5.xml", "BloodAndSmoke_Testset_DD_6.xml", "BloodAndSmoke_Testset_DD_7.xml"),
        ("BloodAndSmoke_Testset_HD_1.xml", "BloodAndSmoke_Testset_HD_2.xml", "BloodAndSmoke_Testset_HD_4.xml",
         "BloodAndSmoke_Testset_HD_5.xml", "BloodAndSmoke_Testset_HD_6.xml", "BloodAndSmoke_Testset_HD_7.2.xml")
    ],
    'ins': [
        ("Instruments_Testset_DD_1.xml", "Instruments_Testset_DD_2.xml", "Instruments_Testset_DD_4.xml",
         "Instruments_Testset_DD_5.xml", "Instruments_Testset_DD_6.xml", "Instruments_Testset_DD_7.xml"),
        ("Instruments_Testset_HD_1.xml", "Instruments_Testset_HD_2.xml", "Instruments_Testset_HD_4.xml",
         "Instruments_Testset_HD_5.xml", "Instruments_Testset_HD_6.xml", "Instruments_Testset_HD_7.2.xml")
    ]
}
test_annotation_root = "/mnt/ceph/tco/TCO-Staff/Projects/SurgOmics_Prospective_AL/Annotations/Test/"
