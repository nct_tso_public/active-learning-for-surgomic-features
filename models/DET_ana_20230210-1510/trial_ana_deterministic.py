import datetime
import os
from shutil import copy2

import numpy as np
import torch
import torch.nn as nn
from torch.optim import SGD
from torch.utils.data import DataLoader, ConcatDataset
from torchvision.models import resnet50, ResNet50_Weights
from tqdm import tqdm

import config
from dataset import MultiRaterAnnotation, TrainSet, transform

MODE = 'ana'
THRESHOLD = .5
BATCH_SIZE = 16
NUM_WORKERS = 8
NUM_EPOCHS = 100
LEARN_RATE = 3e-3
DROPOUT = 0.2
NUM_CLASSES = 2

HEIGHT = 480
WIDTH = 640


def trial():
    name = f"DET"
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    # load data
    sts = TrainSet(
        height=HEIGHT, width=WIDTH,
        mras=[MultiRaterAnnotation(files=files, mode=MODE)
              for files in [f for f, r in config.annotations_per_round[MODE].items() if r == 0]],
        subset=f'Equidistant',
        transform=transform,
    )

    annotations = [f for f, r in config.annotations_per_round[MODE].items() if r > 0]
    mras = [MultiRaterAnnotation(files=files, mode=MODE) for files in annotations]
    eq = TrainSet(
        height=HEIGHT, width=WIDTH,
        mras=mras,
        subset=f'Equidistant',
        transform=transform,
    )
    al = TrainSet(
        height=HEIGHT, width=WIDTH,
        mras=mras,
        subset=f'ActiveLearning/{MODE}',
        transform=transform,
    )

    ds = ConcatDataset([sts, al, eq])
    dl = DataLoader(
        ds,
        shuffle=True,
        batch_size=BATCH_SIZE,
        num_workers=NUM_WORKERS,
        pin_memory=True
    )

    # init model
    net = resnet50(weights=ResNet50_Weights.IMAGENET1K_V2)
    net.fc = nn.Linear(net.fc.in_features, NUM_CLASSES)
    net.to(device)

    # init optim etc
    optimizer = SGD(params=filter(lambda p: p.requires_grad, net.parameters()),
                    lr=LEARN_RATE, weight_decay=1e-5)

    lr_scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer=optimizer,
                                                       max_lr=LEARN_RATE,
                                                       epochs=NUM_EPOCHS,
                                                       steps_per_epoch=len(dl))
    pos_weight = torch.tensor(
        np.clip(
            a=((len(ds) / np.stack(list(sts.labels.values()) + list(al.labels.values()) + list(eq.labels.values())).sum(
                axis=0)) - 1),
            a_min=0,
            a_max=1e6
        )
    )
    criterion = nn.BCEWithLogitsLoss(pos_weight=pos_weight)
    criterion.to(device)

    # init logging
    name += f'_{MODE}_{datetime.datetime.now().strftime("%Y%m%d-%H%M")}'
    os.makedirs(f"out/{name}/", exist_ok=False)
    copy2(os.path.realpath(__file__), f"out/{name}/")
    copy2(os.path.join(os.path.dirname(os.path.realpath(__file__)), "config.py"), f"out/{name}/")

    with open(f"out/{name}/frames.txt", "w") as f:
        for frame in sorted(sts.data):
            print(frame, file=f)
        for frame in sorted(al.data):
            print(frame, file=f)
        for frame in sorted(eq.data):
            print(frame, file=f)

    with open(f"out/{name}/annotations.txt", "w") as f:
        for files in config.annotations_d[MODE]:
            print(", ".join(files), file=f)

    # train
    bar = tqdm(total=NUM_EPOCHS, desc="Epoch")
    for epoch in range(NUM_EPOCHS):
        net.train()
        cumulative_loss = []

        for _, frame, target in dl:
            output = net(frame.to(device))
            loss = criterion(output, target.to(device).float())
            cumulative_loss.append(loss.item())

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            lr_scheduler.step()

        cumulative_loss = np.nanmean(cumulative_loss)
        bar.set_description(desc=f"Epoch, Loss: {cumulative_loss:.3f}")
        bar.update()

    # save
    torch.save(net.state_dict(), f"out/{name}/Model.pt")
    return f"out/{name}/"


if __name__ == '__main__':
    trial()
