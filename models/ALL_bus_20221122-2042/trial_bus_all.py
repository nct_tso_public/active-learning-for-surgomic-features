import datetime
import os
from shutil import copy2

import numpy as np
import torch
import torch.nn as nn
from sklearn.metrics import precision_recall_fscore_support, average_precision_score
from torch.optim import SGD
from torch.utils.data import DataLoader, ConcatDataset
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

import config
from dataset import MultiRaterAnnotation, TrainSet, transform
from model import BayesianResNet18

MODE = 'bus'
THRESHOLD = .5
BATCH_SIZE = 16
NUM_WORKERS = 8
NUM_EPOCHS = 100
LEARN_RATE = 3e-3
DROPOUT = 0.2
NUM_CLASSES = 9


def trial():
    name = f"ALL"
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    # load data
    sts = TrainSet(
        mras=[MultiRaterAnnotation(files=files, mode=MODE)
              for files in [f for f, r in config.annotations_per_round[MODE].items() if r == 0]],
        subset=f'Equidistant',
        transform=transform,
    )

    annotations = [f for f, r in config.annotations_per_round[MODE].items() if r > 0]
    mras = [MultiRaterAnnotation(files=files, mode=MODE) for files in annotations]
    eq = TrainSet(
        mras=mras,
        subset=f'Equidistant',
        transform=transform,
    )
    al = TrainSet(
        mras=mras,
        subset=f'ActiveLearning/{MODE}',
        transform=transform,
    )

    ds = ConcatDataset([sts, al, eq])
    dl = DataLoader(
        ds,
        shuffle=True,
        batch_size=BATCH_SIZE,
        num_workers=NUM_WORKERS,
        pin_memory=True
    )

    # init model
    net = BayesianResNet18(p=DROPOUT, n_classes=NUM_CLASSES)
    net.to(device)
    softmax = torch.nn.Softmax(dim=1)

    # init optim etc
    optimizer = SGD(params=filter(lambda p: p.requires_grad, net.parameters()),
                    lr=LEARN_RATE, weight_decay=1e-5)

    lr_scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer=optimizer,
                                                       max_lr=LEARN_RATE,
                                                       epochs=NUM_EPOCHS,
                                                       steps_per_epoch=len(dl))

    criterion = nn.BCELoss()

    # init logging
    name += f'_{MODE}_{datetime.datetime.now().strftime("%Y%m%d-%H%M")}'
    os.makedirs(f"out/{name}/", exist_ok=False)
    tb = SummaryWriter(f"out/tb/{name}")
    copy2(os.path.realpath(__file__), f"out/{name}/")
    copy2(os.path.join(os.path.dirname(os.path.realpath(__file__)), "config.py"), f"out/{name}/")

    with open(f"out/{name}/frames.txt", "w") as f:
        for frame in sorted(sts.data):
            print(frame, file=f)
        for frame in sorted(al.data):
            print(frame, file=f)
        for frame in sorted(eq.data):
            print(frame, file=f)

    with open(f"out/{name}/annotations.txt", "w") as f:
        for files in config.annotations_d[MODE]:
            print(", ".join(files), file=f)

    # train
    for epoch in tqdm(range(NUM_EPOCHS), desc="Epoch"):
        tb.add_scalar("LR", lr_scheduler.get_last_lr()[0], global_step=epoch)

        net.train()
        y_score = []
        y_true = []
        cumulative_loss = []

        for _, frame, target in dl:
            out = net(frame.to(device))
            blood = softmax(out[:, :5])
            smoke = softmax(out[:, 5:])
            output = torch.concat([blood, smoke], dim=1)

            y_true.extend(target.numpy())
            y_score.extend(output.detach().cpu().numpy())

            loss = criterion(output, target.to(device).float())
            cumulative_loss.append(loss.item())

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            lr_scheduler.step()

        y_score = np.asarray(y_score)
        y_true = np.asarray(y_true)
        cumulative_loss = np.nanmean(cumulative_loss)

        tb.add_scalar("Loss", cumulative_loss, global_step=epoch)
        ap = average_precision_score(y_true=y_true, y_score=y_score, average='macro')
        tb.add_scalar("Average Precision", ap, global_step=epoch)

        y_pred = y_score > THRESHOLD
        p, r, f, _ = precision_recall_fscore_support(y_true=y_true, y_pred=y_pred, average='macro')
        tb.add_scalar("Precision", p, global_step=epoch)
        tb.add_scalar("Recall", r, global_step=epoch)
        tb.add_scalar("F1-Score", f, global_step=epoch)

        label_names = list(config.tags_d[MODE].keys())[:NUM_CLASSES]
        p, r, f, s = precision_recall_fscore_support(y_true=y_true, y_pred=y_pred, average=None)
        tb.add_scalars("PrecisionPerTool", {k: v for k, v in zip(label_names, p)},
                       global_step=epoch)
        tb.add_scalars("RecallPerTool", {k: v for k, v in zip(label_names, r)},
                       global_step=epoch)
        tb.add_scalars("F1PerTool", {k: v for k, v in zip(label_names, f)},
                       global_step=epoch)
        tb.add_scalars("SupportPerTool", {k: v for k, v in zip(label_names, s)},
                       global_step=epoch)

    # save
    torch.save(net.state_dict(), f"out/{name}/Model.pt")
    return f"out/{name}/"


if __name__ == '__main__':
    trial()
